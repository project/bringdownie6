
This is a simple module that creates a block to display a logo and link to 
http://www.bringdownie6.com/

You can see this in operation at my personal web site, http://www.glatz.com

I support their initiative, and believe the world would be a better place if 
more users supported standards compliant open source browsers. Of course this is 
unlikely to happen any time soon, but we can at least try to educate people to 
the advantages of a more neutral browser framework. Developers creating 
sophisticated sites often waste a considerable part of their time trying to make 
sites work on IE, which takes a slightly different approach to the rendering of 
CSS and Javascript than what are accepted standards. If we didn't have to deal 
with these compatability issues, it would free up time for more creative 
pursuits, service to others, more time with family and loved ones, etc.

Sometimes there are reasons why people continue to use non-standard browsers 
like IE. It can be expensive to roll out a differnt browser at a large company, 
for example. Some users *never* change any software installed on their computer. 
Some are lazy, and some are ignorant of the advantages of switching. I believe 
that making the switch would make for a better world.

I don't dislike Microsoft; on the contrary, I am appreciative of their important 
role in creating the modern personal computer industry. I only wish they were 
more open to standards that will benefit users at large, rather than force them 
into using proprietary software. It is important that users realize there are 
alternatives, and that supporting them will both increase productivity and lead 
to a richer user experience.
Requirements


------------
This module requires Drupal 6.0.


Installation
------------
  * copy it to your sites/all/modules path
  * enable the module at admin/build/modules
  * enable/position the block at admin/build/block


Other Resources
---------------
  * Do like mortendk, and charge a tariff for IE6 support, to reflect the extra 
    work involved: http://morten.dk/blog/ie6-tax-now
  
  * The IE6 Update module <http://drupal.org/project/ie6update> gives users a 
    warning if they are using IE6.  
  
  * Join the death march http://iedeathmarch.org/


Author
------
Phil Glatz <drupal@glatz.com>
www.glatz.com
March 21/2009
